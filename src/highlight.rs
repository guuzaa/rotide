use termion::color;

#[derive(PartialEq)]
pub enum Type {
    None,
    Number,
    Match,
}

pub enum HColor {
    RGB(color::Rgb),
    Default,
}

impl Type {
    pub fn to_color(&self) -> HColor {
        match self {
            Type::Number => HColor::RGB(color::Rgb(220, 163, 163)),
            Type::Match => HColor::RGB(color::Rgb(38, 139, 210)),
            _ => HColor::Default,
        }
    }
}

#![warn(clippy::all, clippy::pedantic, clippy::restriction)]
#![allow(
    clippy::missing_docs_in_private_items,
    clippy::implicit_return,
    clippy::shadow_reuse,
    clippy::print_stdout,
    clippy::wildcard_enum_match_arm,
    clippy::else_if_without_else
)]
mod editor;
mod terminal;
mod row;
mod document;
mod history;
mod highlight;

use editor::Editor;
pub use terminal::Terminal;
pub use editor::Position;
pub use editor::SearchDirection;
pub use row::Row;
pub use document::Document;
pub use history::{Edit, History};
pub use highlight::{Type, HColor};

// reference: https://gitlab.redox-os.org/redox-os/termion/-/blob/master/examples/alternate_screen_raw.rs


fn main() {
    Editor::default().run();
}

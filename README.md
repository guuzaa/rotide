
# Rust 实现的文本编辑器

编辑器名字叫作 rotide (`"editor".reverse()`)，
设计理念主要借鉴自 [Hecto](https://www.philippflenker.com/hecto/)。
我对编辑器感兴趣，所以用该程序来复现一些编辑器中的功能，如果你也如此，可以来看看。
我翻译的教程在 https://paulden.site/posts/hecto/ ，翻译还需进一步校对，
如果你感兴趣可以帮忙！

本程序以 [Unlicense](https://unlicense.org/) 分发，你可以随意传播和使用。

实现的功能：
- 编辑：增、删、改、撤回和重做；
- 搜索：前向后向搜索；
- 语法高亮。

**在 Hecto 的基础上，增加了撤回重做功能，并修复了一些小细节。
增加的内容我会在中文教程中更新。**


## 反馈和贡献代码
如果你发现了教程中的错误或者想到了改进代码的好点子可以提交 Issues，共同交流。
